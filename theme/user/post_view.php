<?php 
include('../admin_sup/db.php');

if (isset($_GET['postId'])) 
{
	$postId = $_GET['postId'];
}
else
{
	header('location:index.php');
}


 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="..\..\lib\bootstrap-3.3.6\css\bootstrap.min.css">
    <script src="..\..\lib\bootstrap-3.3.6\js\bootstrap.min.js"></script>
	<title><?php echo $postTitle; ?></title>
</head>
<style>
	.postbox
	{
		margin-top:10px;
		border:1px solid gray;
		width:980px;
		height:600px;
		margin-bottom: 30px;

	}
	.post_header
	{
		border-bottom:1px solid #ccc;
		width:930px;
		height:60px;
		margin:-15px 10px 15px 10px;
	}
	.post_body
	{
		    margin-top:10px;
			margin-left:10px;
	}

</style>
<body>
<div class="col-md-2">
</div>
<div class="col-md-8">
 
<?php 

$sql1=mysqli_query($connect,"SELECT * FROM post Where postId = '$postId' ");
while ($row=mysqli_fetch_array($sql1)) 
{
    $postTitle=$row['postTitle'];
    $postDesc=$row['postDesc'];
    $postCont = $row['postCont'];
    $file1=$row['file1'];
    $file2=$row['file2'];
    $file3=$row['file3'];
    $postDate=$row['postDate'];
    $authorName=$row['authorName'];
?>	
	<div class="postbox ">
	<div class="post_header"><h2><?php echo $postTitle; ?></h2>
	<p style="    margin: -10px 0 10px;">Posted by <span style="color:#06C"><?php echo $authorName; ?> </span> <span style="color:tomato">| Date: <?php echo $postDate; ?></span></p>
	<?php if (!empty($file1)) { ?>
	<img src="../admin_sup/upload/content/<?php	echo $file1; ?>" alt="" width="310px" height="200px" style="margin-top:30px">
	<?php 	} ?>
		<?php if (!empty($file2)) { ?>
	<img src="../admin_sup/upload/content/<?php	echo $file2; ?>" alt="" width="310px" height="200px" style="margin-top:30px">
	<?php 	} ?>
		<?php if (!empty($file3)) { ?>
	<img src="../admin_sup/upload/content/<?php	echo $file3; ?>" alt="" width="310px" height="200px" 
	style="margin-left: 628px;margin-top: -222px;">
	<?php 	} ?>
	</div>
	<div class="post_body">
		<p style="margin-top:250px">
			<?php echo $postCont; ?>
		</p>
	</div>
<?php 
			} 

            ?>
</div>
<div class="col-md-2">
</div>	
</body>
</html>