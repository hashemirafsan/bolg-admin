<?php 

                            /*==================================================================*\

                            ######################################################################

                            #                                                                    #

                            # Copyright 2016 FreeBuck Solutions, All Rights Reserved.           #

                            #                                                                    #

                            # This file may not be redistributed in whole or part.               #

                            #                                                                    #

                            #                                                                    #

                            # ---------------- Apblog IS NOT FREE SOFTWARE ----------------- #

                            #                                                                    #

                            # http://www.freebuck.xyz |                                          #

                            ######################################################################

                            \*==================================================================*/
  include('../admin_sup/db.php');

//paggination start::

$sql1=mysqli_query($connect,"SELECT * FROM post");
$total_row= mysqli_num_rows($sql1);
$num_page=$total_row/3;
$num_of_page=ceil($num_page);
$page1=0;
if(isset($_GET['page']))
{
    $page=$_GET['page'];
    if($page==""||$page=="1")
    {
        $page1=0;
    }
    else
    {
        $page1=($page*3)-3;
    }
}
//end pagination


?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="..\..\lib\bootstrap-3.3.6\css\bootstrap.min.css">
    <script src="..\..\lib\bootstrap-3.3.6\js\bootstrap.min.js"></script>
	<title>Posts</title>
</head>
<style>
	.postbox
	{
		border:1px solid gray;
		width:750px;
		height: 400px;
		margin-bottom: 30px

	}
	.post_header
	{
		border-bottom:1px solid #ccc;
		width:700px;
		height:60px;
		margin:-15px 10px 15px 10px;
	}
	.post_body
	{
		    margin-top:10px;
			margin-left:10px;
	}

</style>
<body>
<div class="col-md-3">
</div>
<div class="col-md-6">
<?php 
$sql2=mysqli_query($connect,"SELECT * FROM post ORDER BY postId DESC LIMIT $page1,3");
while ($row=mysqli_fetch_array($sql2)) 
{
	$postId=$row['postId'];
    $postTitle=$row['postTitle'];
    $postDesc=$row['postDesc'];
    $file1=$row['file1'];
    $file2=$row['file2'];
    $file3=$row['file3'];
    $postDate=$row['postDate'];
    $authorName=$row['authorName'];
    	if (!empty($file1)) 
		{
			$file= $file1;
		}
		if (empty($file1) AND !empty($file2)) 
		{
			$file= $file2;
		}
		if (empty($file1) AND empty($file2) AND !empty($file3)) 
		{
			$file= $file3;
		}
?>

	<div class="postbox ">
	<div class="post_header">
	<h2><?php echo $postTitle; ?></h2>
	<p style="    margin: -10px 0 10px;">Posted by <span style="color:#06C"><?php echo $authorName; ?> </span> <span style="color:tomato">| Date: <?php echo $postDate; ?></span></p>
	</div>
	<div class="post_body">
		<p style="float:left;">
		<img src="../admin_sup/upload/content/<?php	echo $file; ?>" alt="" width="310px" height="200px" style="margin-right:50px">
			<?php echo $postDesc; ?>
		</p>
		<a href="post_view.php?postId=<?php echo $postId; ?>" style="float:right">more..</a>
	</div>
	</div>
<?php 
			} 

            for($i=1;$i<=$num_of_page;$i++)
            {
               ?>
               <ul>
               <li style="list-style:none;float:left;margin-left:50px">
               <a href="index.php?page=<?php echo $i; ?>"><?php echo $i;?></a>
               </li>
               </ul>
               <?php 
            }
            ?>
</div>
<div class="col-md-3">
</div>
</body>
</html>