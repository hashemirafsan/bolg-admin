<?php 
include '../../class/class.header.php';
include '../../class/theme_admin.css.php';
include '../../class/theme_admin.js.php';
?>
<link rel="stylesheet" href="profile.css">

</head>
<body>
    
    <div id="loader"><div id="back"></div></div>
    
    <div class="row">
    <!-- uncomment code for absolute positioning tweek see top comment in css -->
    <!-- <div class="absolute-wrapper"> </div> -->
    <!-- Menu -->
    <?php
    /*
    nav_bar add here 
    */ 
    include '../../class/class.nav_admin.php';
    
    ?>
    <?php
    /*
    edit post function is going to admin folder
    */ 
    include '../../admin/admin_sup/edit-post.php';
    
    ?>
    <!-- Main Content -->
    <div class="container-fluid">
       <div class="side-body">
               <div class="row">
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table" id="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Post Title</th>
                        <th>Name</th>
                        <th>Edit/Update</th>
                         <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?php id(); ?></td>
                        <td><?php title(); ?></td>
                        <td><?php person(); ?></td>
                        <td><?php edit(); ?></td>
                        <td><?php delete(); ?></td>
                    </tr>
                </tbody>
            </table>
            <hr>
        </div>
    </div>
    <script>
        
        $(function () {
    $( '#table' ).searchable({
        striped: true,
        oddRow: { 'background-color': '#f5f5f5' },
        evenRow: { 'background-color': '#fff' },
        searchType: 'fuzzy'
    });
    
});
    </script>
    
<script src="//rawgithub.com/stidges/jquery-searchable/master/dist/jquery.searchable-1.0.0.min.js"></script>

        </div>
    </div>
</div>
<?php 
include '../../class/class.footer.php';
?>

<!---

                             /*==================================================================*\

                            ######################################################################

                            #                                                                    #

                            # Copyright 2016 FreeBuck Solutions, All Rights Reserved.           #

                            #                                                                    #

                            # This file may not be redistributed in whole or part.               #

                            #                                                                    #

                            #                                                                    #

                            # ---------------- Apblog IS NOT FREE SOFTWARE ----------------- #

                            #                                                                    #

                            # http://www.freebuck.xyz |                                          #

                            ######################################################################

                            \*==================================================================*/
                            
                            
 ---->