<?php 
include '../../class/class.header.php';
include '../../class/theme_admin.css.php';
include '../../class/theme_admin.js.php';
?>
<link rel="stylesheet" href="profile.css">

</head>

<?php include '../../admin/admin_sup/user-cont.php';  ?>
<body>
    
    <div id="loader"><div id="back"></div></div>
    
    <div class="row">
    <!-- uncomment code for absolute positioning tweek see top comment in css -->
    <!-- <div class="absolute-wrapper"> </div> -->
    <!-- Menu -->
    <?php
    /*
    nav_bar add here 
    */ 
    include '../../class/class.nav_admin.php';
    
    ?>
    <!-- Main Content -->
    <div class="container-fluid">
       <div class="side-body">
                <table class="table">
  <thead class="thead-inverse" style= "background:black;color:white">
    <tr>
      <th>#</th>
      <th>Username</th>
      <th>Access</th>
      <th>User Post</th>
      <th>View Profile</th>
      <th>Suspend</th>
       <th>Approve/Ban</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row"><?php id(); ?></th>
      <td><?php username(); ?></td>
      <td><?php access(); ?></td>
      <td><?php pro(); ?></td>
      <td><?php view(); ?></td>
      <td><?php suspend(); ?>
      <td><?php approve() ?></td>
    </tr>
  </tbody>
</table>



        </div>
    </div>
</div>
<?php 
include '../../class/class.footer.php';
?>


<!---

                             /*==================================================================*\

                            ######################################################################

                            #                                                                    #

                            # Copyright 2016 FreeBuck Solutions, All Rights Reserved.           #

                            #                                                                    #

                            # This file may not be redistributed in whole or part.               #

                            #                                                                    #

                            #                                                                    #

                            # ---------------- Apblog IS NOT FREE SOFTWARE ----------------- #

                            #                                                                    #

                            # http://www.freebuck.xyz |                                          #

                            ######################################################################

                            \*==================================================================*/
                            
                            
 ---->