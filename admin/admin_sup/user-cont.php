<?php 

                            /*==================================================================*\

                            ######################################################################

                            #                                                                    #

                            # Copyright 2016 FreeBuck Solutions, All Rights Reserved.           #

                            #                                                                    #

                            # This file may not be redistributed in whole or part.               #

                            #                                                                    #

                            #                                                                    #

                            # ---------------- Apblog IS NOT FREE SOFTWARE ----------------- #

                            #                                                                    #

                            # http://www.freebuck.xyz |                                          #

                            ######################################################################

                            \*==================================================================*/
 include 'seen.php';
  
 function id(){
     
     include 'db.php';
      $sql = "select * from user order by id DESC";
      $data = mysqli_query($connect,$sql);
      while($row = mysqli_fetch_array($data)){
          $id = $row['id'];
          echo "<span class='badge badge-purple'>$id</span></a>"."<br><br><br>";
      }
 }
 function username(){
     include 'db.php';
      $sql = "select * from user order by id DESC";
      $data = mysqli_query($connect,$sql);
      while($row = mysqli_fetch_array($data)){
          $user = $row['username'];
          echo "<span class='label label-large label-pink arrowed-right'>$user</span>"."<br><br><br>";
      }
 }
    function access(){
        include 'db.php';
        $sql = "select * from user order by id DESC";
        $data = mysqli_query($connect,$sql);
        while($row = mysqli_fetch_array($data)){
            $access = $row['access'];
            if($access == "active"){
                 echo "<span class='badge badge-success'>$access</span></a>"."<br><br><br>";
            }
           elseif ($access == "request") {
               echo "<span class='badge badge-important'>$access</span></a>"."<br><br><br>";
           }
             elseif ($access == "suspend") {
               echo "<span class='badge badge-warning'>$access</span></a>"."<br><br><br>";
           }
             elseif ($access == "ban") {
               echo "<span class='badge badge-danger'>$access</span></a>"."<br><br><br>";
           }
        }
    }
    
     function pro(){
        include 'db.php';
        $sql = "select * from user order by id DESC";
        $data = mysqli_query($connect,$sql);
        while($row = mysqli_fetch_array($data)){
            $pro = $row['pro'];
            echo "<span class='badge badge-yellow'>$pro</span></a>"."<br><br><br>";
        }
    }
    
     function view(){
        include 'db.php';
        $sql = "select * from user order by id DESC";
        $data = mysqli_query($connect,$sql);
        while($row = mysqli_fetch_array($data)){
            $id = $row['id'];
            $access = $row['access'];
              $i = $_GET['i'];
              $u =$_GET['u'];
            if ($id==$i) 
            {
               echo "<a href='' class='btn btn-sm btn-info' disabled><span class='glyphicon glyphicon-user' ></span> view</a>"."<br><br>";
            }
            else
            {
            echo "<a href='viewprof.php?id=$id&u=$u&i=$i' class='btn btn-sm btn-info'><span class='glyphicon glyphicon-user'></span> view</a>"."<br><br>";
           }
        }
    }
    
    function suspend(){
        include 'db.php';
        $sql = "select * from user order by id DESC";
        $data = mysqli_query($connect,$sql);
        while($row = mysqli_fetch_array($data)){
            $id = $row['id'];
            $access = $row['access'];
            $i = $_GET['i'];
            $u =$_GET['u'];
            if ($id==$i) 
            {
               echo "<a href='' class='btn btn-sm btn-warning' disabled><span class='glyphicon glyphicon-warning-sign' ></span>Suspend</a>"."<br><br>";
            }
            else
            {
            echo "<a href='suspend.php?id=$id&u=$u&i=$i' class='btn btn-sm btn-warning'><span class='glyphicon glyphicon-warning-sign'></span>Suspend</a>"."<br><br>";
            }
        }
    }
    
    
     function approve(){
        include 'db.php';
            $u =$_GET['u'];
            $i = $_GET['i'];
        $sql = "select * from user order by id DESC ";
        $data = mysqli_query($connect,$sql);
        while($row = mysqli_fetch_array($data)){
            $access = $row['access'];
            $id = $row['id'];
         if ($id != $i) {
           if (($access == "request")){
                echo "<a href='approve.php?id=$id&u=$u&i=$i' class='btn btn-sm btn-success'><span class='glyphicon glyphicon-ok'></span> approve</a>"."<br><br>";
            }
            elseif (($access == "suspend")){
                echo "<a href='approve.php?id=$id&u=$u&i=$i' class='btn btn-sm btn-success'><span class='glyphicon glyphicon-ok'></span> Re-allow</a>"."<br><br>";
            }
            elseif (($access == "active" )) {
                 echo "<a href='ban.php?id=$id&u=$u&i=$i' class='btn btn-sm btn-danger'><span class='glyphicon glyphicon-remove'></span> Ban</a>"."<br><br>";
            }
             elseif (($access == "suspend")) {
                 echo "<a href='ban.php?id=$id&u=$u&i=$i' class='btn btn-sm btn-danger'><span class='glyphicon glyphicon-remove'></span> Ban</a>"."<br><br>";
            }
             elseif (($access == "ban")){
                echo "<a href='approve.php?id=$id&u=$u&i=$i' class='btn btn-sm btn-success'><span class='glyphicon glyphicon-ok'></span> Unban</a>"."<br><br>";
            }

          }
        }
    }

?>