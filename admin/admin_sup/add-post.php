<?php

                            /*==================================================================*\

                            ######################################################################

                            #                                                                    #

                            # Copyright 2016 FreeBuck Solutions, All Rights Reserved.           #

                            #                                                                    #

                            # This file may not be redistributed in whole or part.               #

                            #                                                                    #

                            #                                                                    #

                            # ---------------- Apblog IS NOT FREE SOFTWARE ----------------- #

                            #                                                                    #

                            # http://www.freebuck.xyz |                                          #

                            ######################################################################

                            \*==================================================================*/

if (isset($_POST['submit'])) 
{
  $postTitle=$_POST['postTitle'];
  $postDesc=$_POST['postDesc'];
  $postCont=$_POST['postCont'];

  try
  {
    if (empty($postTitle)) 
    {
      throw new Exception("<b style='color:#777'>You should provide a Title!</b>");
      
    }
     if (empty($postDesc)) 
    {
      throw new Exception("<b style='color:#777'>You should provide a Description for your post!</b>");
      
    }
      if (empty($postCont)) 
    {
      throw new Exception("<b style='color:#777'>You should provide a Description for your post's Content!</b>");
      
    }

    
    $sql=mysqli_query($connect,"SELECT max(postId) as max_postId FROM post ");
    while ($row=mysqli_fetch_array($sql)) 
    {
      $nowId=$row['max_postId'];
    }

    $file1=$_FILES['file1']['name'];
    $file1save=$_FILES['file1']['tmp_name'];
    $file1ex=substr($file1, strpos($file1,'.')+1);
    $name1=($nowId+1)."_1.".$file1ex;

   if(isset($file1))
   {
       if (( $file1ex=='jpg') || ( $file1ex=='jpeg') || ( $file1ex=='png')) 
       {
          $move1=move_uploaded_file($file1save,"upload/content/".$name1);
       }
   }

    $file2=$_FILES['file2']['name'];
    $file2save=$_FILES['file2']['tmp_name'];
    $file2ex=substr($file2, strpos($file2,'.')+1);
    $name2=($nowId+1)."_2.".$file2ex;

   if(isset($file2))
   {
    
       if (( $file2ex=='jpg') || ( $file2ex=='jpeg') || ( $file2ex=='png')) 
       {
          $move2=move_uploaded_file($file2save,"upload/content/".$name2);
       }
    }

    $file3=$_FILES['file3']['name'];
    $file3save=$_FILES['file3']['tmp_name'];
    $file3ex=substr($file3, strpos($file3,'.')+1);
    $name3=($nowId+1)."_3.".$file3ex;

    if(isset($file3))
   {
    
       if (( $file3ex=='jpg') || ( $file3ex=='jpeg') || ( $file3ex=='png')) 
       {
          $move3=move_uploaded_file($file3save,"upload/content/".$name3);
       }

    }


$i= $_GET['i'];
$u= $_GET['u'];
$sql2=mysqli_query($connect,"SELECT * FROM user WHERE id='$i' ");
while ($row=mysqli_fetch_array($sql2)) 
{
 $authorName = $row['username'];
}

$postDate=date("M d, Y");
  
        if (empty($file1)&&empty($file2)&&empty($file3)) 
       {
        throw new Exception("<b style='color:#777'>You should provide a Image for this post!</b>");
       }
       elseif (isset($move1)&&!isset($move2)&&!isset($move3))
       {
         $sql1=mysqli_query($connect,"INSERT INTO post(postTitle,postDesc,postCont,file1,authorName,postDate) VALUES('$postTitle','$postDesc','$postCont','$name1','$authorName','$postDate') ");
       }
        elseif (!isset($move1)&&isset($move2)&&!isset($move3))
       {
         $sql1=mysqli_query($connect,"INSERT INTO post(postTitle,postDesc,postCont,file2,authorName,postDate) VALUES('$postTitle','$postDesc','$postCont','$name2','$authorName','$postDate') ");
       }
       
         elseif(!isset($move1)&&!isset($move2)&&isset($move3))
       {
         $sql1=mysqli_query($connect,"INSERT INTO post(postTitle,postDesc,postCont,file3,authorName,postDate) VALUES('$postTitle','$postDesc','$postCont','$name3','$authorName','$postDate') ");
       }
       
          elseif(isset($move1)&&isset($move2)&&!isset($move3))
       {
         $sql1=mysqli_query($connect,"INSERT INTO post(postTitle,postDesc,postCont,file1,file2,authorName,postDate) VALUES('$postTitle','$postDesc','$postCont','$name1','$name2','$authorName','$postDate') ");
       }
          elseif(isset($move1)&&!isset($move2)&&isset($move3))
       {
         $sql1=mysqli_query($connect,"INSERT INTO post(postTitle,postDesc,postCont,file1,file3,authorName,postDate) VALUES('$postTitle','$postDesc','$postCont','$name1','$name3','$authorName','$postDate') ");
       }
          elseif(!isset($move1)&&isset($move2)&&isset($move3))
       {
         $sql1=mysqli_query($connect,"INSERT INTO post(postTitle,postDesc,postCont,file2,file3,authorName,postDate) VALUES('$postTitle','$postDesc','$postCont','$name2','$name3','$authorName','$postDate',) ");
       }
          elseif(isset($move1)&&isset($move2)&&isset($move3))
       {
         $sql1=mysqli_query($connect,"INSERT INTO 
          post(postTitle,postDesc,postCont,file1,file2,file3,authorName,postDate) VALUES('$postTitle','$postDesc','$postCont','$name1','$name2','$name3','$authorName','$postDate') ");
       }
     
       
       
       }
  catch(Exception $e)
  {
    $error=$e->getMessage();
  }
}

?>